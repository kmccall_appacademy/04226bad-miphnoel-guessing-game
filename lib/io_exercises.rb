# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  number = rand(1..100)
  guess, guesses = nil, 0

  until guess == number
    guess = get_a_guess
    guesses += 1
    say_higher_or_lower(guess, number)
  end

  report_victory(number, guesses)
end

# Debatably worthwhile abstraction
def report_victory(number, guesses)
  puts "#{number} is right! It took you #{guesses} guesses this time"
end

def get_a_guess
  puts "guess a number between 1 and 100"
  gets.chomp.to_i
end

# Passing two arguments worth slightly simpler guessing_game method?
# Considered making comparison in guessing_game, having separate
# too_high and too_low methods, but would still need to pass 'guess'
def say_higher_or_lower(guess, number)
  if guess > number
    puts "Sorry, #{guess} is too high"
  elsif guess < number
    puts "Nope, #{guess} is too low"
  end
end

def file_shuffler
  print "Enter file name: "
  input_name = gets.chomp

  lines = File.readlines(input_name)
  shuffled_lines = lines.shuffle

  File.open("#{input_name}-shuffled.txt", "w") do |f|
    shuffled_lines.each do |line|
      f.puts line
    end
  end
end
#
# if __FILE__ == $PROGRAM_NAME
#   file_shuffler
# end
